---
layout: layouts/base.njk
eleventyNavigation:
  key: Brewing
  order: 3
---
# Brewing

<img src="/img/sob3-BW.png" width="200">

## Founder of South Omaha Brewers Homebrew Club

In 2009 started the **South Omaha Brewers Homebrew Club**

<img src="/img/FH badge logo 2019.png" width="200">

## Co-owner and Co-brewer at Farnam House Brewing Company

Founded **Farnam House Brewing Company** which opened in 2014

<img src="/img/WBC16_silver.jpg" width="200">

Our _Biere de Garde (Scarlet Rooster)_ won a  **Silver Medal** in Belgian and French-Style Ale at the 2016 World Beer Cup

<!--
Category: 57 Belgian- and French-Style Ale - 42 Entries
Gold: Barrel Aged Woodthrush, Little Fish Brewing Co., Athens, OH
Silver: Farnam House Biere de Garde, Farnam House Brewing Co., Omaha, NE
Bronze: Domaine DuPage, Two Brothers Brewing Co., Warrenville, IL
-->






