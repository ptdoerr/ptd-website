---
layout: layouts/base.njk
eleventyNavigation:
  key: Certifications
  order: 5
---
# **Certifications**


<table>
  <tr>
    <!--<td><img src="/img/w3schools-30.jpg" width="30"/></td>-->
    <td><b>W3Schools</b></td>
    <td><b><a href="https://verify.w3schools.com/1N7UCXFLV9">SQL</a></b></td>
  </tr>
  <tr>
    <!--<td><img src="/img/w3schools-30.jpg" width="30"/></td>-->
    <td><b>W3Schools</b></td>
    <td><b><a href="https://verify.w3schools.com/1N9ALEM0E5">Python</a></b></td>
  </tr>
  <tr>
    <!--<td><img src="/img/w3schools-30.jpg" width="30"/></td>-->
    <td><b>W3Schools</b></td>
    <td><b><a href="https://verify.w3schools.com/1N7UTP5ZNG">Pandas</a></b></td>
  </tr>
  <tr>
    <!--<td><img src="/img/w3schools-30.jpg" width="30"/></td>-->
    <td><b>W3Schools</b></td>
    <td><b><a href="https://verify.w3schools.com/1N8MWD9JMS">NumPy</a></b></td>
  </tr>
  <tr>
    <!--<td><img src="/img/w3schools-30.jpg" width="30"/></td>-->
    <td><b>Meta/Coursera</b></td>
    <td><b><a href="https://www.coursera.org/verify/GSEQRPHH562C">React Basics</a></b></td>
  </tr>
  <tr>
    <!--<td><img src="/img/w3schools-30.jpg" width="30"/></td>-->
    <td><b>Meta/Coursera</b></td>
    <td><b><a href="https://www.coursera.org/verify/UXRC9VVHQQ6F">Javascript Programming</a></b></td>
  </tr>
    <td><b>Meta/Coursera</b></td>
    <td><b><a href="https://coursera.org/verify/6K46E63T6FZ8">Advanced React</a></b></td>
  <tr>

  

</table>
