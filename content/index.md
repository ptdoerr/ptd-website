---
layout: layouts/base.njk
eleventyNavigation:
  key: About
  order: 1
---
# Phil Doerr
Bellevue, NE 

(402) 871-3036

[phil.doerr@gmail.com](mailto://phil.doerr@gmail.com)

[Linkedin](https://www.linkedin.com/in/phil-doerr-1294b31/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3BJsXqFlGUThSV1eTZ3lhlEg%3D%3D)

[GitHub](https://github.com/ptdoerr)

<img src="/img/phil.jpg" width="200">

Experienced Software Engineer and Architect with extensive  experience developing complex systems for the Department of Defense. Looking to re-enter the software engineering field after a brief period owning a small business. I have spent several years learning current development methodologies and frameworks including user-interface frameworks, cloud technologies and DevSecOps.




