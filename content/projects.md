---
layout: layouts/base.njk
eleventyNavigation:
  key: Projects
  order: 2
---
# Projects

**[COVID Dashboard](../project-pages/cdash/index.html)**

Built in Python running in Jupyter Notebook. Uses daily updates of [John's Hopkins  dataset of US County cases on Github](https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv). Full set is normalized to county population data and 7 day moving average is calculated. 

Graphs are built showing current cases and graph of past cases for local counties. Notebook has dynamic interface to include any set of US counties and variable time period.

A map is also built showing color coded current US count case levels. There is also a tool to build animated gifs to show geographical case progression over variable time periods.

These images are updated whenever the notebook is run. Working to refactor logic to use in Flask webapp or an AWS Lambda that could accessed by this site. The code is available on [GitHub](https://github.com/ptdoerr/COVIDdashboard).

**[Spring Boot Image Server](http://66.228.55.215:8080/)**

I built an image browser using Spring Boot to display map images that have been saved to a private AWS S3 bucket using the AWS Java API. The client is written in HTML/Javascript and it's secured using OAuth2 through a GitHub login. It's running as a Docker image on a Linode server. View code on [GitHub](https://github.com/ptdoerr/spring-image-server).

I'm currently working on a React version of the UI as part of my [website conversion to React/Next.js](https://ptdoerr.gitlab.io/ptd-nextjs-website/tech). Eventually the new version will also have access to the full county level dataset loaded onto a AWS RDS MySQL instance available through Spring Boot JPA services. Eventually the client will have interactive graphing available using D3. View code on [GitHub](https://github.com/ptdoerr/CovidSpringDashboard).

**[Bird Flu Outbreaks](https://lookerstudio.google.com/reporting/137050b6-8fd6-4eac-9491-b5cef776a253)**

Dynamic display of CDC Avian Flu Outbreak data. Data is automatically loaded daily into a Google Spreadsheet. This data is fused with UD county location data and displayed in Google Data Studio.

