---
layout: layouts/base.njk
eleventyNavigation:
  key: Resume
  order: 7
---
# Resume

[//]: # (This probably needs to be refreshed periodically)
[//]: # (change /edit=share  to /export?format=pdf )

Direct PDF conversion of Google Doc. 

[My Resume](https://docs.google.com/document/d/1vZE-ANik0Tq0aqpP5DImW2i-g8zt2ffteu5eKr-yaR4/export?format=pdf)